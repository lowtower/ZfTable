<?php

declare(strict_types=1);
/**
 * ZfTable ( Module for Zend Framework 2).
 *
 * @copyright Copyright (c) 2013 Piotr Duda dudapiotrek@gmail.com
 * @license   MIT License
 */

namespace ZfTable\Params;

use ZfTable\AbstractCommon;

abstract class AbstractAdapter extends AbstractCommon
{
    /**
     * Get configuration of table.
     *
     * @return \ZfTable\Options\ModuleOptions
     */
    public function getOptions()
    {
        return $this->getTable()->getOptions();
    }
}
